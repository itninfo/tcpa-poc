const firebaseConfig = {
  apiKey: "AIzaSyB92y_-xjJ7_sq1g7nsOh8X_MIrlket2MU",
  authDomain: "image-store-e24d5.firebaseapp.com",
  projectId: "image-store-e24d5",
  storageBucket: "image-store-e24d5.appspot.com",
  messagingSenderId: "918467122478",
  appId: "1:918467122478:web:9322b6b40dc7cac7f696bf",
  measurementId: "G-P24QHCP7DM",
};
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app(); // if already initialized, use that one
}

const storage = firebase.storage();

const node = document.getElementById("my-node");
const subBtn = document.getElementById("subBtn");
const hiddenLink = document.getElementById("hiddenLink");

function dataURLtoFile(dataurl, filename) {
  let arr = dataurl.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = window.atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
}

const handleUpload = async (image) => {
  const newImageFile = await dataURLtoFile(image, `new-image-${Math.random()}`);

  const uploadTask = storage
    .ref(`images/${newImageFile.name}`)
    .put(newImageFile);

  await uploadTask.on(
    "state_changed",
    (error) => {
      console.log(error);
    },
    () => {
      storage
        .ref("images")
        .child(newImageFile.name)
        .getDownloadURL()
        .then((url) => {
          console.log(url);
        });
    }
  );

  hiddenLink.click();
};

const filter = (node) => {
  const classArray = node.classList ? [...node.classList] : [];
  const exclusionClasses = ["remove-me"];
  return !exclusionClasses.some((classname) => classArray.includes(classname));
};

subBtn.addEventListener("click", function () {
  htmlToImage
    .toJpeg(node, { filter: filter })
    .then(function (dataUrl) {
      handleUpload(dataUrl);
    })
    .catch(function (error) {
      console.error("oops, something went wrong!", error);
    });
});
